import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';
class App extends Component {
  state = {
    persons: [
      {name:'Dirsza',age:22},
      {name:'Raka', age:21},
      {name:'Ricky', age:22}
    ],
    otherState:'some other value'
  }
  switchNameHandler = (newName) =>{
    //console.log('was clicked')
    // DONT DO THIS: this.state.persons[0].name='Safiro';
    this.setState({
      persons:[
      {name:newName,age:22},
      {name:'Raka', age:21},
      {name:'Ricky', age:22}
    ]})
  }
  nameChangedHandler= (event) =>{
    this.setState({
      persons:[
      {name:'safiro',age:22},
      {name:event.target.value, age:21},
      {name:'Ricky', age:22}
    ]})
  }
  render() {
    const style ={
      backroundColor:'white',
      font:'inherit',
      border:'1px solid blue',
      padding:'8px'
    }
    return (
      <div className="App">
        <h1>Hi, I'm a React App</h1>
        <p>This is really working</p>
        <button style={style} onClick={()=> this.switchNameHandler('safiro!!!')}>Switch Name</button>
        <Person 
        name={this.state.persons[0].name}
         age={this.state.persons[0].age}/>     
        <Person
         name={this.state.persons[1].name} 
         age={this.state.persons[1].age}
         click={this.switchNameHandler.bind(this,'saf!')}
         changed={this.nameChangedHandler}> My Hobbies: Suka Makan </Person>     
        <Person 
        name={this.state.persons[2].name} 
         age={this.state.persons[2].age}/>     
         </div>
    );
  //  return React.createElement('div',{className:'App'},React.createElement('h1',null,'Does this work now?'))
  }
}

export default App;
